#pragma once

#include <ntifs.h>

#include "CommonNames.h"
#include "new.h"
#include "delete.h"
#include "UndocumentedImports.h"
#include "InjectedShellcode.h"

extern EX_RUNDOWN_REF g_rundown_protection;

namespace apc_routines
{
	void kernel_free_kapc(PKAPC apc, PKNORMAL_ROUTINE*, PVOID*, PVOID*, PVOID*);
	void rundown_free_kapc(PKAPC apc);
	void normal_inject_code(PVOID, PVOID, PVOID);
}