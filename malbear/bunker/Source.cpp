#include <ntifs.h>
#include <ntddk.h>

#include "new.h"
#include "UndocumentedImports.h"
#include "bunker.h"
#include "BunkerCommon.h"
#include "APCRoutines.h"

void OnProcessNotify(HANDLE, HANDLE process_id, BOOLEAN create);
void OnThreadNotify(_Inout_ HANDLE ProcessId, _In_ HANDLE ThreadId, _Inout_opt_ BOOLEAN Create);
void BunkerUnload(PDRIVER_OBJECT DriverObject);

Globals g_Globals;

NewProcessesCache* g_new_processes_list;
EX_RUNDOWN_REF g_rundown_protection;
typedef PVOID(*fnPsGetProcessWow64Process)(PEPROCESS Process);

extern fnPsGetProcessWow64Process PsGetProcessWow64Process;

extern "C" 
NTSTATUS DriverEntry(PDRIVER_OBJECT DriverObject, PUNICODE_STRING)
{
	DriverObject->DriverUnload = BunkerUnload;

	DbgPrint("TEST PRINT!!!!!!!!!!\n");
	//PKAPC apc = new (NonPagedPool, DRIVER_TAG) KAPC;
	g_Globals.new_processes_cache = (NewProcessesCache*)::ExAllocatePoolWithTag(PagedPool, sizeof(NewProcessesCache), DRIVER_TAG);
	g_Globals.new_processes_cache->Initialize();

	if (!g_Globals.new_processes_cache) {
		DbgPrint("[-] Failed to create a remote thread creation list.\n");

		return STATUS_INSUFFICIENT_RESOURCES;
	}

	auto status = ::PsSetCreateProcessNotifyRoutine(OnProcessNotify, FALSE);

	if (!NT_SUCCESS(status)) {
		ExFreePool(g_new_processes_list);

		DbgPrint("[-] Failed to create a process notify routine.\n");

		return status;
	}

	status = ::PsSetCreateThreadNotifyRoutine(OnThreadNotify);

	if (!NT_SUCCESS(status)) {
		::PsSetCreateProcessNotifyRoutine(OnProcessNotify, TRUE);
		ExFreePool(g_new_processes_list);

		DbgPrint("[-] Failed to create a thread notify routine.\n");

		return status;
	}

	::ExInitializeRundownProtection(&g_rundown_protection);

	DbgPrint("[+] Loaded LibraryHookingDriver successfully.\n");

	return STATUS_SUCCESS;
}

void BunkerUnload(PDRIVER_OBJECT DriverObject)
{
	UNREFERENCED_PARAMETER(DriverObject);

	::PsSetCreateProcessNotifyRoutine(OnProcessNotify, TRUE);
	::PsRemoveCreateThreadNotifyRoutine(OnThreadNotify);

	g_Globals.new_processes_cache->unInitialize();
	ExFreePool(g_Globals.new_processes_cache);

	::ExWaitForRundownProtectionRelease(&g_rundown_protection);

	DbgPrint("[+] Unloaded LibraryHookingDriver successfully.\n");
}

void OnProcessNotify(HANDLE, HANDLE ProcessId, BOOLEAN Create)
{
	DbgPrint("[!] Process Creation Detected\n");
	if (Create) {
		PEPROCESS process;
		auto status = ::PsLookupProcessByProcessId(ProcessId, &process);
		DbgPrint("[?] Looking For Process\n");
		if (NT_SUCCESS(status)) {
			DbgPrint("[!] Found Process\n");
			if (!PsIsProtectedProcess(process)) { //!::fnPsGetProcessWow64Process(process) && 
				g_Globals.new_processes_cache->add_process(HandleToULong(ProcessId));
				DbgPrint("!!! ADDED PROCESS !!! PID: %d\n", HandleToULong(ProcessId));
			}
		}
	}
}

void OnThreadNotify(_Inout_ HANDLE ProcessId, _In_ HANDLE ThreadId, _Inout_opt_ BOOLEAN Create)
{
	if (Create && g_Globals.new_processes_cache->is_newly_created(HandleToLong(ProcessId))) 
	{
		g_Globals.new_processes_cache->remove_process(HandleToLong(ProcessId));
		DbgPrint("[!] 1\n");
		PETHREAD thread;
		::PsLookupThreadByThreadId(ThreadId, &thread);
		PKAPC apc = (PKAPC)::ExAllocatePoolWithTag(NonPagedPool, sizeof(PKAPC), DRIVER_TAG);
		//PKAPC apc = new (NonPagedPool, DRIVER_TAG) KAPC;
		if (!apc)
		{
			DbgPrint("[-] Could not queue a kernel APC due to insufficient memory.\n");
			return;
		}

		DbgPrint("[!] 2\n");
		::KeInitializeApc(
			apc,
			thread,
			OriginalApcEnvironment,
			&apc_routines::kernel_free_kapc,
			&apc_routines::rundown_free_kapc,
			&apc_routines::normal_inject_code,
			KernelMode,
			nullptr
		);
		DbgPrint("[!] 3\n");
		if (::ExAcquireRundownProtection(&g_rundown_protection)) {
			auto inserted = ::KeInsertQueueApc(
				apc,
				nullptr,
				nullptr,
				0
			);
			DbgPrint("[!] 4\n");
			if (!inserted) {
				ExFreePool(apc);
				::ExReleaseRundownProtection(&g_rundown_protection);

				DbgPrint("[-] Could not insert a kernel APC.\n");

				return;
			}
		}
	}
}
