#pragma once

#define DRIVER_PREFIX "Bunker: "
#define DEVICE_NAME RTL_CONSTANT_STRING(L"\\Device\\bunker")
#define SYMBOLIC_LINK RTL_CONSTANT_STRING(L"\\??\\bunker") 

#define DRIVER_TAG 0x42554e4b // BUNK