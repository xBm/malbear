#include "remote_thread_creation_list.h"

void RemoteThreadCreationList::Initialize()
{
	_list.Initialize();
}

void RemoteThreadCreationList::unInitialize()
{
	_list.unInitialize();
}

void RemoteThreadCreationList::add_remote_thread_creation(ULONG thread_id, ULONG process_id, ULONG creator_process_id)
{
	AutoLock<FastMutex> lock(_list.get_mutex());

	RemoteThreadCreationEntry* entry = (RemoteThreadCreationEntry*)::ExAllocatePoolWithTag(PagedPool, sizeof(RemoteThreadCreationEntry*), DRIVER_TAG);
	//auto entry = new (PagedPool, DRIVER_TAG) RemoteThreadCreationEntry();

	if (!entry) 
	{
		DbgPrint("[-] Failed to log a remote thread creation detected due to insufficient memory.\n");
		return;
	}

	entry->remote_thread_creation.thread_id = thread_id;
	entry->remote_thread_creation.process_id = process_id;
	entry->remote_thread_creation.creator_process_id = creator_process_id;

	_list.insert(&entry->list_entry);
}

RemoteThreadCreationEntry* RemoteThreadCreationList::remove_remote_thread_creation()
{
	AutoLock<FastMutex> lock(_list.get_mutex());

	auto entry = _list.remove_tail();

	if (entry != _list.get_head()) 
		return CONTAINING_RECORD(entry, RemoteThreadCreationEntry, list_entry);

	return nullptr;
}