#pragma once

#define DRIVER_PREFIX "Bunker: "
#define DEVICE_NAME RTL_CONSTANT_STRING(L"\\Device\\bunker")
#define SYMBOLIC_LINK RTL_CONSTANT_STRING(L"\\??\\bunker") 

#define DRIVER_TAG 0x42554e4b // BUNK

#include "FastMutex.h"
#include "new_processes_cache.h"
#include "remote_thread_creation_list.h"

// A data structure that holds the global state of the driver
struct Globals
{
	LIST_ENTRY ItemsHead;
	int ItemCount;
	NewProcessesCache* new_processes_cache;
	RemoteThreadCreationList* remote_thread_creation_list;
	FastMutex Mutex;
};

template<typename T>
struct FullItem
{
	LIST_ENTRY Entry;
	T Data;
};
