#include "new_processes_cache.h"

void NewProcessesCache::Initialize()
{
	_list.Initialize();
}

void NewProcessesCache::unInitialize()
{
	_list.unInitialize();
}

void NewProcessesCache::add_process(ULONG process_id)
{
	AutoLock<FastMutex> lock(_list.get_mutex());

	NewProcessEntry* entry = (NewProcessEntry*)::ExAllocatePoolWithTag(PagedPool, sizeof(NewProcessEntry*), DRIVER_TAG);
	//auto entry = new (PagedPool, DRIVER_TAG) NewProcessEntry();

	if (!entry) {
		DbgPrint("[-] Failed to log a new process creation detected due to insufficient memory.\n");
		return;
	}

	DbgPrint("[+] Inserting process to cache.\n");
	entry->process_id = process_id;
	_list.insert(&entry->list_entry);
}

void NewProcessesCache::remove_process(ULONG process_id)
{
	AutoLock<FastMutex> lock(_list.get_mutex());

	auto head = _list.get_head();
	auto current = head->Flink;

	while (current != head) 
	{
		auto entry = CONTAINING_RECORD(current, NewProcessEntry, list_entry);

		if (process_id == entry->process_id) 
		{
			_list.remove(current);
			return;
		}

		current = current->Flink;
	}
}

bool NewProcessesCache::is_newly_created(ULONG process_id)
{
	AutoLock<FastMutex> lock(_list.get_mutex());

	auto head = _list.get_head();
	auto current = head->Flink;

	DbgPrint("Process Count: %d\n", _list.get_count());

	while (current != head)
	{
		auto entry = CONTAINING_RECORD(current, NewProcessEntry, list_entry);

		if (entry)
		{
			DbgPrint("Process In List: %p\n", (void*)entry);
			if (process_id == entry->process_id)
				return true;
		}
		
		current = current->Flink;
	}

	return false;
}
