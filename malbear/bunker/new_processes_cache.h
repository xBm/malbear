#pragma once

#include <ntifs.h>
#include "new.h"
#include "list.h"
#include "AutoLock.h"
#include "BunkerCommon.h"
#include "CommonNames.h"

struct NewProcessEntry
{
	LIST_ENTRY list_entry;
	ULONG process_id = 0;
};

class NewProcessesCache
{
public:
	void Initialize();
	void unInitialize();

	void add_process(ULONG process_id);
	void remove_process(ULONG process_id);
	bool is_newly_created(ULONG process_id);

private:
	List _list;
};