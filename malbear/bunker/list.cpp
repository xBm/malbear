#include "list.h"
#include "delete.h"

void List::Initialize()
{
	::InitializeListHead(&_head);
	_mutex.Init();
	_count = 0;
	DbgPrint("List Count at Initialization: %d\n", _count);
}

void List::unInitialize()
{
	while (!::IsListEmpty(&_head)) {
		auto entry = ::RemoveTailList(&_head);
		ExFreePool(entry);
	}
}

void List::insert(LIST_ENTRY* entry)
{
	if (_count >= 1024) { // move to constant / registry key
		auto head = ::RemoveHeadList(&_head);
		ExFreePool(head);

		_count--;
	}
	DbgPrint("Insert Count: %d\n", _count);
	::InsertTailList(&_head, entry);
	_count++;
}

void List::remove(LIST_ENTRY* entry)
{
	::RemoveEntryList(entry);
	ExFreePool(entry);

	_count--;
}

LIST_ENTRY* List::remove_tail()
{
	if (_count > 0) {
		_count--;
		return ::RemoveTailList(&_head);
	}

	return nullptr;
}

LIST_ENTRY* List::get_head()
{
	return &_head;
}

FastMutex& List::get_mutex()
{
	return _mutex;
}

ULONG List::get_count()
{
	return _count;
}