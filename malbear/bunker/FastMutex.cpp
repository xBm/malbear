#include "FastMutex.h"

void FastMutex::Init()
{
	ExInitializeFastMutex(&_mutex);
	DbgPrint("!!!! Fast Mutex Constructor !!!!\n");
}

void FastMutex::Lock()
{
	ExAcquireFastMutex(&_mutex);
}

void FastMutex::Unlock()
{
	ExReleaseFastMutex(&_mutex);
}
