#pragma once
#include <ntddk.h>

#include "FastMutex.h"

class List
{
public:
	void Initialize();
	void unInitialize();

	void insert(LIST_ENTRY* entry);
	void remove(LIST_ENTRY* entry);

	LIST_ENTRY* remove_tail();

	LIST_ENTRY* get_head();
	FastMutex& get_mutex();
	ULONG get_count();

private:
	LIST_ENTRY _head;
	FastMutex _mutex;
	ULONG _count;
};