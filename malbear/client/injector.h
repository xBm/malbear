#pragma once
#include <Windows.h>
#include <iostream>

#include "easyhook.h"

#if _WIN64
#pragma comment(lib, "EasyHook64.lib")
#else
#pragma comment(lib, "EasyHook32.lib")
#endif

#define DLL_PATH L"exodus.dll"

class Injector
{
public:
	static void inject(DWORD pid);
};

