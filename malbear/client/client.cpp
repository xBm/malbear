// client.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <vector>
#include <algorithm>
#include <thread>
#include "injector.h"
#include <psapi.h>

#define PROCESSES_BUFFER_SIZE 1024

int main()
{
    DWORD current_pid = GetCurrentProcessId();
    std::vector<DWORD> injected_pids;
    while (true)
    {
        DWORD aProcesses[PROCESSES_BUFFER_SIZE], cbNeeded, cProcesses;
        unsigned int i;

        if (EnumProcesses(aProcesses, sizeof(aProcesses), &cbNeeded))
        {
            // Calculate how many process identifiers were returned.
            cProcesses = cbNeeded / sizeof(DWORD);
            //std::cout << "PIDS:\n" << "-----------------------------------------------" << std::endl;
            for (i = 0; i < cProcesses; i++)
            {

                if (aProcesses[i] != 0 && aProcesses[i] != current_pid)
                {
                    if (std::find(injected_pids.begin(), injected_pids.end(), aProcesses[i]) == injected_pids.end())
                    {
                        try
                        {
                            injected_pids.push_back(aProcesses[i]);
                            std::thread(Injector::inject, aProcesses[i]).detach();
                            //Injector::inject(aProcesses[i]);
                        }
                        catch(...)
                        {
                            std::cout << "Wasn't able to inject DLL to process with pid " << aProcesses[i] << std::endl;
                        }
                    }
                }
            }
            //std::cout << "-----------------------------------------------" << std::endl;
            
            //std::cout << "INJECTED PIDS:" << std::endl;
            int i = 0;
            for (auto& pid : injected_pids)
            {
                if (std::find(std::begin(aProcesses), std::end(aProcesses), pid) == std::end(aProcesses))
                {
                    injected_pids.erase(injected_pids.begin() + i);
                }
                i++;
            }
        }
    }
    return 0;
}
