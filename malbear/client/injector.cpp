#include "injector.h"

void Injector::inject(DWORD pid)
{

	// Inject dllToInject into the target process Id, passing 
	// freqOffset as the pass through data.
	NTSTATUS nt = RhInjectLibrary(
		pid,   // The process to inject into
		0,           // ThreadId to wake up upon injection
		EASYHOOK_INJECT_DEFAULT,
		NULL, // 32-bit
		(WCHAR*)DLL_PATH,		 // 64-bit not provided
		NULL, // data to send to injected DLL entry point
		NULL// size of data to send
	);

	if (!nt)
	{
		std::wcout << L"Library injected successfully to process with pid " << pid << std::endl;
	}
}