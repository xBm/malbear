﻿using System;
using System.Management;
using System.Diagnostics;

public class EventWatcherPolling
{
    public static int Main(string[] args)
    {
        WqlEventQuery query =
            new WqlEventQuery("__InstanceCreationEvent",
            new TimeSpan(0, 0, 0, 1),
            "TargetInstance isa \"Win32_Process\"");

        ManagementEventWatcher watcher = new ManagementEventWatcher();
        watcher.Query = query;

        while (true)
        {
            try
            {
                ManagementBaseObject e = watcher.WaitForNextEvent();

                ProcessStartInfo startInfo = new ProcessStartInfo("Injector.exe");
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                startInfo.Arguments = ((ManagementBaseObject)e["TargetInstance"])["ProcessId"].ToString();
                Process.Start(startInfo);

            }
            catch (Exception e)
            {
                Console.WriteLine("{0} Exception caught.", e);
                break;
            }
        }

        watcher.Stop();
        watcher.Dispose();
        return 0;
    }
}