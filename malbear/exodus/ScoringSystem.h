#pragma once
#include <Windows.h>
#include <unordered_map>

enum anomalyScore
{
	VERY_LOW = 5,
	LOW = 10,
	MEDIUM_LOW = 25,
	MEDIUM = 45,
	MEDIUM_HIGH = 65,
	HIGH = 80,
	EXTREME = 90,
	KILL = 100
};

class ScoringSystem
{
private:
	static std::unordered_map<int, int> _threatCount;

public:
	static int _score;
	static void addThreat(int threatID);
};
