#include "Hook.h"

Hook::Hook()
{
	//TCHAR szFileName[MAX_PATH];
	//GetModuleFileName(NULL, szFileName, MAX_PATH);

	//MessageBox(NULL, szFileName, L"InjectedDLL", MB_OK);
	DeleteFileHook* deleteFileHook = new DeleteFileHook();
	deleteFileHook->hook();
	_hooks.push_back(deleteFileHook);

	MoveFileHook* moveFileHook = new MoveFileHook();
	moveFileHook->hook();
	_hooks.push_back(moveFileHook);

	CreateDirectoryHook* createDirectoryHook = new CreateDirectoryHook();
	createDirectoryHook->hook();
	_hooks.push_back(createDirectoryHook);

	CreateFileHook* createFileHook = new CreateFileHook();
	createFileHook->hook();
	_hooks.push_back(createFileHook);
}

Hook::~Hook()
{
	for(auto hook = _hooks.begin(); hook != _hooks.end(); ++hook)
		delete *hook;
}