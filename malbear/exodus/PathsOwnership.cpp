#include "PathsOwnership.h"

std::vector<std::string> PathsOwnership::_pathsHolder;

bool PathsOwnership::addPath(const std::string& newPath)
{
	size_t adressLen = newPath.length();
	for (const std::string curr : _pathsHolder)
	{
		if (adressLen > curr.length())
		{
			std::string adressBeginning = newPath.substr(STRING_BEGINNING, adressLen);
			if (adressBeginning == newPath) return false;
		}
	}
	_pathsHolder.push_back(newPath);
	return true;
}
bool PathsOwnership::checkForPath(const std::string& newPath)
{
	size_t adressLen = newPath.length();
	for (const std::string curr : _pathsHolder)
	{
		if (curr.length() <= adressLen) return false; //TODO : DONT RETURN

		std::string currBeginning = curr.substr(STRING_BEGINNING, adressLen);
		if (currBeginning == newPath) return true;
	}
	return false;
}