#include "ScoringSystem.h"

int ScoringSystem::_score = 0;

void ScoringSystem::addThreat(int threatID)
{
	_score += threatID;
	if (_score >= 100)
	{
		MessageBox(NULL, L"Eliminating Threat.", L"!!! THREAT DETECTED !!!", MB_OK);
		// Terminating the process
		DWORD pid = GetCurrentProcessId();
		HANDLE hnd;
		hnd = OpenProcess(SYNCHRONIZE | PROCESS_TERMINATE, TRUE, pid);
		TerminateProcess(hnd, 0);
	}
}
