#pragma once
#include <iostream>
#include "HookHandler.h"
#include "helper.h"

class CreateDirectoryHook : public HookHandler
{
protected:
	HOOK_TRACE_INFO _createDirectoryAHook = { NULL };
	HOOK_TRACE_INFO _createDirectoryWHook = { NULL };
	ULONG ACLEntriesA[1] = { 0 };
	ULONG ACLEntriesW[1] = { 0 };

public:
	void hook();
	~CreateDirectoryHook();
};

