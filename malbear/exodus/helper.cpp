#include "helper.h"

std::vector<std::string> Helper::split(std::string& str, const char* seperator)
{
    char* cstr = const_cast<char*>(str.c_str());
    char* current;
    char* p;
    std::vector<std::string> arr;
    current = strtok_s(cstr, seperator, &p);
    while (current != NULL) 
    {
        arr.push_back(current);
        current = strtok_s(NULL, seperator, &p);
    }
    return arr;
}

double Helper::FileTimeToSeconds(const FILETIME& t)
{
    return LARGE_INTEGER{ t.dwLowDateTime, (long)t.dwHighDateTime }.QuadPart * 1e-7;
}

bool Helper::check_known_extension(const std::string& extension)
{
    for (auto& knownExtension : KNOWN_RANSOMWARE_EXTENSIONS)
    {
        if (extension == knownExtension)
            return true;
    }
    return false;
}