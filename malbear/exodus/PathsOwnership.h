#pragma once
#include <string>
#include <vector>

#define STRING_BEGINNING 0

class PathsOwnership
{
private:

public:
	static std::vector<std::string> _pathsHolder;
	static bool addPath(const std::string& newPath);
	static bool checkForPath(const std::string& newPath);
};

