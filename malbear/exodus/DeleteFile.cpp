#include "DeleteFile.h"
#include "PathsOwnership.h"

BOOL WINAPI DeleteFileAHook(_In_ LPCSTR lpFileName)
{
	Helper helper;
	char filename[MAX_PATH];
	DWORD size = GetModuleFileNameA(NULL, filename, MAX_PATH);
	std::string currentFile = std::string(filename);

	if (currentFile == std::string(lpFileName))
	{
		ScoringSystem::addThreat(MEDIUM);
	}
	/*
	*	Checking if the original path is under the ownership of the process
	*/
	else if (!PathsOwnership::checkForPath(currentFile))
	{
		ScoringSystem::addThreat(MEDIUM_LOW);
	}

	return DeleteFileA(lpFileName);
}

BOOL WINAPI DeleteFileWHook(_In_ LPCWSTR lpFileName)
{
	Helper helper;
	char filename[MAX_PATH];
	DWORD size = GetModuleFileNameA(NULL, filename, MAX_PATH);
	std::string currentFile = std::string(filename);

	std::wstring wcharFilename = lpFileName;
	if (currentFile == std::string(wcharFilename.begin(), wcharFilename.end()))
	{
		ScoringSystem::addThreat(HIGH);
	}
	/*
	*	Checking if the original path is under the ownership of the process
	*/
	else if (!PathsOwnership::checkForPath(currentFile))
	{
		ScoringSystem::addThreat(MEDIUM_LOW);
	}

	return DeleteFileW(lpFileName);
}

void DeleteFileHook::hook()
{
	NTSTATUS result = LhInstallHook(
		GetProcAddress(GetModuleHandle(TEXT("kernel32")), "DeleteFileA"),
		DeleteFileAHook,
		NULL,
		&_deleteFileAHook);

	result = LhInstallHook(
		GetProcAddress(GetModuleHandle(TEXT("kernel32")), "DeleteFileW"),
		DeleteFileWHook,
		NULL,
		&_deleteFileWHook);

	// Disable the hook for the provided threadIds, enable for all others
	LhSetExclusiveACL(ACLEntriesA, 1, &_deleteFileAHook);
	LhSetExclusiveACL(ACLEntriesW, 1, &_deleteFileWHook);
}

DeleteFileHook::~DeleteFileHook()
{
	LhUninstallHook(&_deleteFileAHook);
	LhWaitForPendingRemovals();
	LhUninstallHook(&_deleteFileWHook);
	LhWaitForPendingRemovals();
}
