#pragma once
#include <iostream>
#include <unordered_map>
#include "HookHandler.h"
#include "helper.h"

static std::vector<std::string> newFileNames;
static std::unordered_map<std::string, int> extensions;

class MoveFileHook : public HookHandler
{
protected:
	HOOK_TRACE_INFO _moveFileAHook = { NULL };
	HOOK_TRACE_INFO _moveFileWHook = { NULL };
	ULONG ACLEntriesA[1] = { 0 };
	ULONG ACLEntriesW[1] = { 0 };

public:
	void hook();
	~MoveFileHook();
};