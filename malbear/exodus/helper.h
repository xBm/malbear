#pragma once
#include <Windows.h>
#include <iostream>
#include <string>
#include <vector>
#include <string.h>
#include "HookHandler.h"

class Helper
{
public:
	static std::vector<std::string> split(std::string& str, const char* seperator);
	static double FileTimeToSeconds(const FILETIME& t);
	static bool check_known_extension(const std::string& extension);
};

