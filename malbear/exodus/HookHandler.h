#pragma once
#include <Windows.h>
#include "easyhook.h"
#include "ScoringSystem.h"

#if _WIN64
#pragma comment(lib, "EasyHook64.lib")
#else
#pragma comment(lib, "EasyHook32.lib")
#endif

const std::vector<std::string> KNOWN_RANSOMWARE_EXTENSIONS = { "ecc", "ezz", "exx", "zzz", "xyz",\
"aaa", "abc", "ccc", "vvv", "xxx", "ttt", "micro", "encrypted", "locked", "crypto", "_crypt", "crinf",\
"r5a", "XRNT", "XTBL", "crypt", "R16M01D05", "pzdc", "good", "LOL!", "OMG!", "RDM", "RRK", "encryptedRSA",\
"crjoker", "EnCiPhErEd", "LeChiffre", "keybtc@inbox_com", "0x0", "bleep", "1999", "vault", "HA3", "toxcrypt",\
"magic", "SUPERCRYPT", "CTBL", "CTB2", "locky", "wcry", "aesir", "asasin", "diablo6", "loptr", "odin", "osiris"\
"shit", "thor", "ykcol", "zepto", "zzzz", "WCRY"};

/// <summary>
/// Abstract class that contains the basic of every handler to hooked function
/// </summary>

class HookHandler
{
public:
	virtual void hook() = 0;
};

