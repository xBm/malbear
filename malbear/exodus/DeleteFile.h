#pragma once
#include <iostream>
#include "HookHandler.h"
#include "helper.h"

class DeleteFileHook : public HookHandler
{
protected:
	HOOK_TRACE_INFO _deleteFileAHook = { NULL };
	HOOK_TRACE_INFO _deleteFileWHook = { NULL };
	ULONG ACLEntriesA[1] = { 0 };
	ULONG ACLEntriesW[1] = { 0 };

public:
	void hook();
	~DeleteFileHook();
};