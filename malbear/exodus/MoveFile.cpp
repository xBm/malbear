#include "MoveFile.h"
#include "PathsOwnership.h"

BOOL MoveFileAHook(LPCSTR lpExistingFileName, LPCSTR lpNewFileName)
{
	// CHECK IF CHANGING NAME OF OLDER FILES

	std::string existingPathWithName = lpExistingFileName;
	std::string newPathWithName = lpNewFileName;

	/*
	*	Checking if the original path is under the ownership of the process
	*/
	if (!PathsOwnership::checkForPath(existingPathWithName))
	{
		ScoringSystem::addThreat(MEDIUM_LOW);
	}

	std::string existingPath = existingPathWithName.substr(0, existingPathWithName.find_last_of("\\"));
	std::string newPath = newPathWithName.substr(0, newPathWithName.find_last_of("\\"));

	if (existingPath == newPath)
	{
		double thisFileTime = 0;
		double targetFileTime = 0;

		HANDLE thisFile = CreateFileA(__FILE__, GENERIC_READ, NULL, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		HANDLE targetFile = CreateFileA(lpExistingFileName, GENERIC_READ, NULL, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		FILETIME creationTime, lpLastAccessTime, lastWriteTime;

		GetFileTime(thisFile, &creationTime, &lpLastAccessTime, &lastWriteTime);
		thisFileTime = Helper::FileTimeToSeconds(creationTime);
		GetFileTime(targetFile, &creationTime, &lpLastAccessTime, &lastWriteTime);
		targetFileTime = Helper::FileTimeToSeconds(creationTime);

		if (thisFileTime > targetFileTime)
		{
			ScoringSystem::addThreat(VERY_LOW);
		}
	}

	if (existingPath.length() == newPath.length())
	{
		std::string filename = Helper::split(newPathWithName, "\\").back();
		newFileNames.push_back(filename);

		if (newFileNames.size() > 10)
		{
			int count = 0;
			std::string firstName = newFileNames[0];
			for (auto& name : newFileNames)
			{
				if (name != firstName && name.length() == firstName.length())
				{
					count++;
					if (count > 15)
						ScoringSystem::addThreat(HIGH);
				}
			}
		}
	}

	std::string prevExtension = existingPathWithName.substr(existingPathWithName.find_last_of(".") + 1);
	std::string newExtension = newPathWithName.substr(newPathWithName.find_last_of(".") + 1);

	if (prevExtension != newExtension)
	{
		if (Helper::check_known_extension(newExtension))
			ScoringSystem::addThreat(KILL);

		if (extensions.find(newExtension) != extensions.end())
		{
			extensions[newExtension]++;
			if (extensions[newExtension] > 5)
				ScoringSystem::addThreat(HIGH);
		}
		else
			extensions[newExtension] = 1;
	}

	return MoveFileA(lpExistingFileName, lpNewFileName);
}

BOOL MoveFileWHook(LPCWSTR lpExistingFileName, LPCWSTR lpNewFileName)
{
	// CHECK IF CHANGING NAME OF OLDER FILES

	std::wstring existingPathWithNameW = lpExistingFileName;
	std::string existingPathWithName = std::string(existingPathWithNameW.begin(), existingPathWithNameW.end());
	std::wstring newPathWithNameW = lpNewFileName;
	std::string newPathWithName = std::string(newPathWithNameW.begin(), newPathWithNameW.end());

	/*
	*	Checking if the original path is under the ownership of the process
	*/
	if (!PathsOwnership::checkForPath(existingPathWithName))
	{
		ScoringSystem::addThreat(MEDIUM_LOW);
	}

	std::string existingPath = existingPathWithName.substr(0, existingPathWithName.find_last_of("\\"));
	std::string newPath = newPathWithName.substr(0, newPathWithName.find_last_of("\\"));

	if (existingPath == newPath)
	{
		double thisFileTime = 0;
		double targetFileTime = 0;

		HANDLE thisFile = CreateFileA(__FILE__, GENERIC_READ, NULL, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		HANDLE targetFile = CreateFileA(existingPathWithName.c_str(), GENERIC_READ, NULL, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		FILETIME creationTime, lpLastAccessTime, lastWriteTime;

		GetFileTime(thisFile, &creationTime, &lpLastAccessTime, &lastWriteTime);
		thisFileTime = Helper::FileTimeToSeconds(creationTime);
		GetFileTime(targetFile, &creationTime, &lpLastAccessTime, &lastWriteTime);
		targetFileTime = Helper::FileTimeToSeconds(creationTime);

		if (thisFileTime > targetFileTime)
		{
			ScoringSystem::addThreat(VERY_LOW);
		}
	}

	if (existingPath.length() == newPath.length())
	{
		std::string filename = Helper::split(newPathWithName, "\\").back();
		newFileNames.push_back(filename);

		if (newFileNames.size() > 10)
		{
			int count = 0;
			std::string firstName = newFileNames[0];
			for (auto& name : newFileNames)
			{
				if (name != firstName && name.length() == firstName.length())
				{
					count++;
					if (count > 15)
						ScoringSystem::addThreat(HIGH);
				}
			}
		}
	}

	std::string prevExtension = existingPathWithName.substr(existingPathWithName.find_last_of(".") + 1);
	std::string newExtension = newPathWithName.substr(newPathWithName.find_last_of(".") + 1);

	if (prevExtension != newExtension)
	{
		MessageBoxA(NULL, newExtension.c_str(), "New Extension", MB_OK);
		if (Helper::check_known_extension(newExtension))
			ScoringSystem::addThreat(KILL);

		if (extensions.find(newExtension) != extensions.end())
		{
			extensions[newExtension]++;
			if (extensions[newExtension] > 5)
				ScoringSystem::addThreat(HIGH);
		}
		else
			extensions[newExtension] = 1;
	}

	return MoveFileW(lpExistingFileName, lpNewFileName);
}

void MoveFileHook::hook()
{
	NTSTATUS result = LhInstallHook(
		GetProcAddress(GetModuleHandle(TEXT("kernel32")), "MoveFileA"),
		MoveFileAHook,
		NULL,
		&_moveFileAHook);

	result = LhInstallHook(
		GetProcAddress(GetModuleHandle(TEXT("kernel32")), "MoveFileW"),
		MoveFileWHook,
		NULL,
		&_moveFileWHook);

	// Disable the hook for the provided threadIds, enable for all others
	LhSetExclusiveACL(ACLEntriesA, 1, &_moveFileAHook);
	LhSetExclusiveACL(ACLEntriesW, 1, &_moveFileWHook);
}

MoveFileHook::~MoveFileHook()
{
	LhUninstallHook(&_moveFileAHook);
	LhWaitForPendingRemovals();
	LhUninstallHook(&_moveFileWHook);
	LhWaitForPendingRemovals();
}
