#pragma once
#include <vector>
#include "DeleteFile.h"
#include "MoveFile.h"
#include "CreateDirectoryHook.h"
#include "CreateFile.h"

class Hook
{
private:
	std::vector<HookHandler*> _hooks;

public:
	Hook();
	~Hook();
};

