#include "CreateFile.h"

HANDLE CreateFileAHook(
	LPCSTR                lpFileName,
	DWORD                 dwDesiredAccess,
	DWORD                 dwShareMode,
	LPSECURITY_ATTRIBUTES lpSecurityAttributes,
	DWORD                 dwCreationDisposition,
	DWORD                 dwFlagsAndAttributes,
	HANDLE                hTemplateFile
)
{
	std::string filename = lpFileName;
	std::string fileExtension = filename.substr(filename.find_last_of(".") + 1);

	for (auto extension : KNOWN_RANSOMWARE_EXTENSIONS)
	{
		if (fileExtension == extension)
		{
			ScoringSystem::addThreat(KILL);
		}
	}

	return CreateFileA(lpFileName, dwDesiredAccess, dwShareMode, lpSecurityAttributes, dwCreationDisposition, dwFlagsAndAttributes, hTemplateFile);
}

HANDLE CreateFileWHook(
	LPCWSTR               lpFileName,
	DWORD                 dwDesiredAccess,
	DWORD                 dwShareMode,
	LPSECURITY_ATTRIBUTES lpSecurityAttributes,
	DWORD                 dwCreationDisposition,
	DWORD                 dwFlagsAndAttributes,
	HANDLE                hTemplateFile
)
{
	std::string filename(std::wstring(lpFileName).begin(), std::wstring(lpFileName).end());
	std::string fileExtension = filename.substr(filename.find_last_of(".") + 1);

	for (auto extension : KNOWN_RANSOMWARE_EXTENSIONS)
	{
		if (fileExtension == extension)
		{
			ScoringSystem::addThreat(KILL);
		}
	}

	return CreateFileW(lpFileName, dwDesiredAccess, dwShareMode, lpSecurityAttributes, dwCreationDisposition, dwFlagsAndAttributes, hTemplateFile);
}

void CreateFileHook::hook()
{
	NTSTATUS result = LhInstallHook(
		GetProcAddress(GetModuleHandle(TEXT("kernel32")), "CreateFileA"),
		CreateFileAHook,
		NULL,
		&_createFileAHook);

	result = LhInstallHook(
		GetProcAddress(GetModuleHandle(TEXT("kernel32")), "CreateFileW"),
		CreateFileWHook,
		NULL,
		&_createFileWHook);

	// Disable the hook for the provided threadIds, enable for all others
	LhSetExclusiveACL(ACLEntriesA, 1, &_createFileAHook);
	LhSetExclusiveACL(ACLEntriesW, 1, &_createFileWHook);
}

CreateFileHook::~CreateFileHook()
{
	LhUninstallHook(&_createFileAHook);
	LhWaitForPendingRemovals();
	LhUninstallHook(&_createFileWHook);
	LhWaitForPendingRemovals();
}