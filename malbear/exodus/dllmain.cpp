// dllmain.cpp : Defines the entry point for the DLL application.
#include <Windows.h>
#include "Hook.h"
#include <stdio.h>

Hook* hook;

extern "C" void __declspec(dllexport) __stdcall NativeInjectionEntryPoint(REMOTE_ENTRY_INFO * inRemoteInfo);

void __stdcall NativeInjectionEntryPoint(REMOTE_ENTRY_INFO* inRemoteInfo)
{
    hook = new Hook();
	return;
}

BOOL APIENTRY DllMain(HMODULE hModule, DWORD ul_reason_for_call, LPVOID lpReserved) 
{
    switch (ul_reason_for_call) 
    {
    case DLL_PROCESS_DETACH:
        delete hook;
        break;
    }
    return TRUE;
}
