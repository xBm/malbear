#include "CreateDirectoryHook.h"
#include "PathsOwnership.h"

BOOL CreateDirectoryAHook(LPCSTR lpPathName, LPSECURITY_ATTRIBUTES lpSecurityAttributes)
{
	std::string newDirPath = lpPathName;
	PathsOwnership::addPath(newDirPath);

	return CreateDirectoryA(lpPathName, lpSecurityAttributes);
}

BOOL CreateDirectoryW(LPCWSTR lpPathName, LPSECURITY_ATTRIBUTES lpSecurityAttributes)
{
	std::wstring temp = lpPathName;
	std::string newDirPath = std::string(temp.begin(), temp.end());
	PathsOwnership::addPath(newDirPath);

	return CreateDirectoryW(lpPathName, lpSecurityAttributes);
}

void CreateDirectoryHook::hook()
{
	NTSTATUS result = LhInstallHook(
		GetProcAddress(GetModuleHandle(TEXT("kernel32")), "CreateDirectoryA"),
		CreateDirectoryAHook,
		NULL,
		&_createDirectoryAHook);

	result = LhInstallHook(
		GetProcAddress(GetModuleHandle(TEXT("kernel32")), "CreateDirectoryW"),
		CreateDirectoryW,
		NULL,
		&_createDirectoryWHook);

	// Disable the hook for the provided threadIds, enable for all others
	LhSetExclusiveACL(ACLEntriesA, 1, &_createDirectoryAHook);
	LhSetExclusiveACL(ACLEntriesW, 1, &_createDirectoryWHook);
}

CreateDirectoryHook::~CreateDirectoryHook()
{
	LhUninstallHook(&_createDirectoryAHook);
	LhWaitForPendingRemovals();
	LhUninstallHook(&_createDirectoryWHook);
	LhWaitForPendingRemovals();
}