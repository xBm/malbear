#pragma once
#include <iostream>
#include "HookHandler.h"
#include "helper.h"

class CreateFileHook : public HookHandler
{
protected:
	HOOK_TRACE_INFO _createFileAHook = { NULL };
	HOOK_TRACE_INFO _createFileWHook = { NULL };
	ULONG ACLEntriesA[1] = { 0 };
	ULONG ACLEntriesW[1] = { 0 };

public:
	void hook();
	~CreateFileHook();
};