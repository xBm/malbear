#include <iostream>
#include <string>
#include <cstring>

#include "easyhook.h"

#if _WIN64
#pragma comment(lib, "EasyHook64.lib")
#else
#pragma comment(lib, "EasyHook32.lib")
#endif

int main(int argc, char* argv[])
{
	DWORD processId = atoi(argv[1]);

	WCHAR dllToInject[26] = L"exodus.dll";

	// Inject dllToInject into the target process Id, passing 
	// freqOffset as the pass through data.
	std::cout << processId << std::endl;
	NTSTATUS nt = RhInjectLibrary(
		processId,   // The process to inject into
		0,           // ThreadId to wake up upon injection
		EASYHOOK_INJECT_DEFAULT,
		NULL, // 32-bit
		dllToInject,		 // 64-bit not provided
		NULL, // data to send to injected DLL entry point
		NULL// size of data to send
	);

	return 0;
}